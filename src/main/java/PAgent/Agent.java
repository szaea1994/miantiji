/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the destroy Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PAgent;

import PMaze.Maze;
import PCommon.Types.Operator;
import PCommon.Types.Heuristic;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Szentpéteri Annamária
 */
public abstract class Agent {
/* ---- CLASS VARIABLES ---- */    
    FieldOperators m_Operators;
    Heuristic m_CurrentHeuristic;
    
/* ---- ABSTRACT CLASS METHODS ---- */
    
    
    
/* ---- NON-ABSTRACT CLASS METHODS ---- */
    public Point yourTurn(Point _CurrentPos){
        return _CurrentPos;
    }
    
    ArrayList<Operator> getUsableOperators(int _X, int _Y){
        return m_Operators.getOperators(_X, _Y);
    }
    
    public void setUsableOperators(Maze _Maze){
        //TODO
    }
}
