/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the destroy Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PAgent;
import java.awt.Point;

/**
 *
 * @author Szentpéteri Annamária
 */
public class ObjectAgent extends Agent {
/* ---- CLASS VARIABLES ---- */


/* ---- CONTSTRUCTORS ---- */    
    public ObjectAgent(){
    }
    
/* ---- GETTER METHODS ---- */
    
/* ---- SETTER METHODS ---- */
    
/* ---- OTHER METHODS ---- */
    /**
     *
     * @param _CurrentPos
     * @return
     */
    @Override
    public Point yourTurn(Point _CurrentPos) {
        return _CurrentPos;
    }
}
