/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the destroy Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PAgent;

import PCommon.Types;
import PCommon.Types.Operator;
import PCommon.Types.Heuristic;
import PCommon.Types.Algorithm;
import PMaze.Maze;
import java.awt.Point;
import static java.lang.Math.abs;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Szentpéteri Annamária
 */
public class MIAgent extends Agent {
/* ---- CLASS VARIABLES ---- */    
    private Heuristic m_Heuristic;
    private int m_Depth;
    private Algorithm m_Algorithm;
    private HashSet<Point> m_SeenFields = null;
    private HashSet<Point> m_SeenObjects = null;
    private ArrayList<ArrayList<Integer>> m_FieldHeuristic = null;
    private HashSet<Point> m_InvalidFields = null;
    private int m_Height;
    private int m_Width;
    private int m_InitValue;


/* ---- CONTSTRUCTORS ---- */    
    public MIAgent(){
        //create inicial Agent
        m_FieldHeuristic = new ArrayList<>();
    }
    
    
/* ---- GETTER METHODS ---- */   
    public HashSet<Point> getSeenFields() {
        //return the array of seen fields 
        return m_SeenFields;
    }

    @Override
    ArrayList<Operator> getUsableOperators(int _X, int _Y) {
        throw new UnsupportedOperationException("TODO XXX"); //To change body of generated methods, choose Tools | Templates.
    } 
    
    public HashSet<Point> getSeenObjects() {
        //return the array of seen fields 
        return m_SeenObjects;
    }
    
    
/* ---- SETTER METHODS ---- */ 
    public void setFieldHeuristic(int _W, int _H, Heuristic _Heuristic){
        if (_W < 0 || _H < 0){
            _W = 0;
            _H = 0;
        }
        m_Width = _W;
        m_Height = _H;
        
        m_InitValue = _W * _H + 2;
        
        if (!m_FieldHeuristic.isEmpty()){
            m_FieldHeuristic.clear();
        }
        
        //A legnagyobb lehetséges lépésszámtól nagyobb értékkel inicializálok
        //minden mezőt
        initHeuristic(_W, _H);
        
        //heurisztikától függően dun dun dun
        m_Heuristic = _Heuristic;
        switch (m_Heuristic){
            case DEFAULT:
                genDefaultHeuristic();
                break;
            case DISTANCE:
                genDistanceHeuristic(_W - 1, _H - 1, 0);
                break;
            case DISTANCE_OBJECT:
                genDistanceObjectHeuristic(_W - 1, _H - 1, 0);
                break;
            case OBJECT:
                genObjectHeuristic();
                break;
        }
    }
    
    public void setAlgorithm(Algorithm _Algorithm){
        m_Algorithm = _Algorithm;
    }
    
    public void setDepth(int d){
        m_Depth = d;
    }
    
/* ---- OTHER METHODS ---- */ 
    /**
     *
     * @param _CurrentPos
     * @param _CurrVisibleFields
     * @param _PosOfVisibleObj
     * @param _InvalidField
     * @return
     */    
    public Point yourTurn(Point _CurrentPos, HashSet<Point> _CurrVisibleFields, HashSet<Point> _PosOfVisibleObj, Point _InvalidField) {
        //A Control szerinti érvénytelen mezők listájának kezelése
        if (_InvalidField == null){
            if (m_InvalidFields != null){
                m_InvalidFields.clear();
            }
            m_InvalidFields = null;
        }
        else{
            if (m_InvalidFields == null){
                m_InvalidFields = new HashSet<>();
            }
            m_InvalidFields.add(_InvalidField);
        }
             
        return calcNextStep(_CurrentPos, _CurrVisibleFields);
    }
    
    private Point calcNextStep(Point _CurrentPos, HashSet<Point> _CurrVisibleFields){
        Point result = null;
        
        //felismeri hogy nem juthat ki
        if (m_FieldHeuristic.get(_CurrentPos.x).get(_CurrentPos.y) == m_InitValue){
            result = Types.GIVE_UP;
        }
        else{ //algoritmustól és mélységtől függő tervezés
            switch (m_Algorithm){
                case CURRENT_POSITION:
                    result = currentPosition(_CurrentPos);
                    break;
                case CURRENT_SEEN:
                    result = currentSeen(_CurrVisibleFields);
                    break;
                case ALL_SEEN:
                    result = allSeen();
                    break;
            }
        }
        
        //Ha újra az érvénytelen mezőt akarná kigenerálni akkor másikat kell választania!
            if (m_InvalidFields != null && m_InvalidFields.size() > 0){
                for(Point invalidField: m_InvalidFields){
                    if (invalidField.equals(result)){
                        //calcNewField valahogy
                    }
                }
            }
        
        return result;
    }
    
    private Point currentPosition(Point _Point){
        int i = _Point.x;
        int j = _Point.y;
        Point point;                
        Point result = null;
        double minValue = m_InitValue;
        double value;
        
        //north
        if (!Maze.getInstance().getFieldNorthBorder(i, j)){
            if (j-1 >= 0){
                point = new Point (i, j-1);
                value = calcRate(point, 1, _Point);
                if (value < minValue){
                    minValue = value;
                    result = point;
                }
            }
        }
        //west
        if (!Maze.getInstance().getFieldWestBorder(i, j)){
            if (i-1 >= 0){
                point = new Point (i-1, j);
                value = calcRate(point, 1, _Point);
                if (value < minValue){
                    minValue = value;
                    result = point;
                }
            }
        }
        //east
        if (!Maze.getInstance().getFieldEastBorder(i, j)){
            if (i+1 < m_Width){
                point = new Point (i+1, j);
                value = calcRate(point, 1, _Point);
                if (value < minValue){
                    minValue = value;
                    result = point;
                }
            }
        }
        //south
        if (!Maze.getInstance().getFieldSouthBorder(i, j)){
            if (j+1 < m_Height){
                point = new Point (i, j+1);
                value = calcRate(point, 1, _Point);
                if (value < minValue){
                    minValue = value;
                    result = point;
                }
            }
        }
        
        if (minValue == -1){
            m_Depth--;
        }
 
        return result;
    }
    
    private double calcRate(Point _Point, int _Depth, Point _CallerPoint){
        //objektum blokkolja az utat
        if (m_FieldHeuristic.get(_Point.x).get(_Point.y) < 0){
            return m_InitValue;
        }
        
        //megtalálta a kijáratot
        if (_Point.equals(new Point(m_Width - 1, m_Height - 1))){
            return -1.0;
        }
        
        int i = _Point.x;
        int j = _Point.y;
        Point point;
        HashSet<Double> returnedValues = new HashSet<>();
        double result = m_InitValue;
        
        if (_Depth < m_Depth){
            //ha nem null a caller akkor ügyelni kell rá hogy arra felé ne menjen az algoritmus
            //csak addig hívjon meg újabb 
            
            //north
            if (!Maze.getInstance().getFieldNorthBorder(i, j)){
                if (j-1 >= 0){
                    point = new Point(i, j-1);
                    if (!point.equals(_CallerPoint)){
                        returnedValues.add(calcRate(point, _Depth + 1, _Point));
                    }
                }
            }
            //west
            if (!Maze.getInstance().getFieldWestBorder(i, j)){
                if (i-1 >= 0){
                    point = new Point(i-1, j);
                    if (!point.equals(_CallerPoint)){
                        returnedValues.add(calcRate(point, _Depth + 1, _Point));
                    }
                }
            }
            //east
            if (!Maze.getInstance().getFieldEastBorder(i, j)){
                if (i+1 < m_Width){
                    point = new Point(i+1, j);
                    if (!point.equals(_CallerPoint)){
                        returnedValues.add(calcRate(point, _Depth + 1, _Point));
                    }
                }
            }
            //south
            if (!Maze.getInstance().getFieldSouthBorder(i, j)){
                if (j+1 < m_Height){
                    point = new Point(i, j+1);
                    if (!point.equals(_CallerPoint)){
                        returnedValues.add(calcRate(point, _Depth + 1, _Point));
                    }
                }
            }
        
            //ki kell választani, hogy melyik a legjobb és azt visszaadni
            //legkisebb heurisztikájú mezők kiválasztása
            if (returnedValues.size() > 0){
                for(Double value: returnedValues){
                    if (value < result){
                        result = value;
                    }
                }
            }
        }
        else{
            //saját értéke, heurisztika és a kijárattól vett távolság kombinációja
            //heurisztika * 10000-rel normalizált kijárattól vett távolságs
            double heuristic = m_FieldHeuristic.get(_Point.x).get(_Point.y);
            double distanceRate = ((m_Width - _Point.x) * (m_Width - _Point.x) + (m_Height - _Point.y) * (m_Height - _Point.y)) / 10000.0;
            result = heuristic + distanceRate;
        }
        
        return result;
    }
    
    private Point currentSeen(HashSet<Point> _Seen){
        return null;
    } 
    
    private Point allSeen(){
        return null;
    }
    
    public void addToSeenFields(HashSet<Point> _NewSeenFields){
        if (_NewSeenFields != null){
            if (m_SeenFields == null){
                m_SeenFields = new HashSet<>(_NewSeenFields);
            }
            else{            
                for(Point newPoint: _NewSeenFields){
                    if (!m_SeenFields.contains(newPoint)){
                        m_SeenFields.add(newPoint);
                    }
                }            
            }
        }
    }
    
    public void addToSeenObjects(HashSet<Point> _NewSeenObjects){
        boolean recalcHeuristic = false;
        
        if (_NewSeenObjects != null){
            if (m_SeenObjects == null){
                m_SeenObjects = new HashSet<>(_NewSeenObjects);
                recalcHeuristic = true;
            }
            else{            
                for(Point newPoint: _NewSeenObjects){
                    if (!m_SeenObjects.contains(newPoint)){
                        m_SeenObjects.add(newPoint);
                        recalcHeuristic = true;
                    }
                }
            }
        }
        
        if (recalcHeuristic){
            switch (m_Heuristic){
                case DISTANCE_OBJECT:
                    initHeuristic(m_Width, m_Height);
                    genDistanceObjectHeuristic(m_Width - 1, m_Height - 1, 0);
                    break;
                case OBJECT:
                    initHeuristic(m_Width, m_Height);
                    genObjectHeuristic();
                    break;
            }
        }
    }
    
    private void initHeuristic(int _W, int _H){
        m_FieldHeuristic.clear();
        
        for (int i = 0; i < _W; i++){
            m_FieldHeuristic.add(new ArrayList<>());
            
            for(int j = 0; j < _H; j++){
                m_FieldHeuristic.get(i).add(j, m_InitValue);
            }
        }
    }
    
    //nem kell befrissíteni objektum esetén
    private void genDefaultHeuristic() {
        for (int i = 0; i < m_FieldHeuristic.size(); i++){
            for(int j = 0; j < m_FieldHeuristic.get(i).size(); j++){
                m_FieldHeuristic.get(i).set(j, 1);
            }
        }
    }
    
    //nem kell befrissíteni objektum esetén
    private void genDistanceHeuristic(int i, int j, int d) {
        //ellenőrzés
        if (m_FieldHeuristic.get(i).get(j) <= d){
            return;
        }        
        
        //beállítás
        m_FieldHeuristic.get(i).set(j, d);
        
        //függvény meghívása a környező mezőkre
        //north
        if (!Maze.getInstance().getFieldNorthBorder(i, j)){
            if (j-1 >= 0){
                genDistanceHeuristic(i, j-1, d+1);
            }
        }
        //east
        if (!Maze.getInstance().getFieldEastBorder(i, j)){
            if (i+1 < m_Width){
                genDistanceHeuristic(i+1, j, d+1);
            }
        }
        //south
        if (!Maze.getInstance().getFieldSouthBorder(i, j)){
            if (j+1 < m_Height){
                genDistanceHeuristic(i, j+1, d+1);
            }
        }
        //west
        if (!Maze.getInstance().getFieldWestBorder(i, j)){
            if (i-1 >= 0){
                genDistanceHeuristic(i-1, j, d+1);
            }
        }
    }

    //be kell frissíteni objektum esetén
    private void genDistanceObjectHeuristic(int i, int j, int d) {
        //ellenőrzés
        if (m_FieldHeuristic.get(i).get(j) <= d){
            return;
        }
        
        //beállítás
        m_FieldHeuristic.get(i).set(j, d);
        
        //ha objektumos mező, akkor -1-re kell állítani
        boolean isObject = false;
        
        if (m_SeenObjects != null){
            Point field = new Point(i, j);
            for(Point p: m_SeenObjects){
                if (field.equals(p)){
                    m_FieldHeuristic.get(i).set(j, -1*m_InitValue);
                    isObject = true;
                    break;
                }
            }
        }
        
        //ha objektumos mező, akkor nem kell meghívni rá a függvényeket
        if (!isObject){
            //függvény meghívása a környező mezőkre
            //north
            if (!Maze.getInstance().getFieldNorthBorder(i, j)){
                if (j-1 >= 0){
                    genDistanceObjectHeuristic(i, j-1, d+1);
                }
            }
            //east
            if (!Maze.getInstance().getFieldEastBorder(i, j)){
                if (i+1 < m_Width){
                    genDistanceObjectHeuristic(i+1, j, d+1);
                }
            }
            //south
            if (!Maze.getInstance().getFieldSouthBorder(i, j)){
                if (j+1 < m_Height){
                    genDistanceObjectHeuristic(i, j+1, d+1);
                }
            }
            //west
            if (!Maze.getInstance().getFieldWestBorder(i, j)){
                if (i-1 >= 0){
                    genDistanceObjectHeuristic(i-1, j, d+1);
                }
            }
        }
    }

    //be kell frissíteni objektum esetén
    private void genObjectHeuristic() {
        for (int i = 0; i < m_FieldHeuristic.size(); i++){
            for(int j = 0; j < m_FieldHeuristic.get(i).size(); j++){
                m_FieldHeuristic.get(i).set(j, 1);
            }
        }
        
        if (m_SeenObjects != null){
            for (Point p: m_SeenObjects){
                m_FieldHeuristic.get(p.x).set(p.y, -1*m_InitValue);
            }
        }
    }
    
}
