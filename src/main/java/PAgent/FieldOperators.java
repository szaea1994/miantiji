/*
 * Copyright (C) 2016 User
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PAgent;

import PCommon.Types.Operator;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author User
 */
class FieldOperators{
    
    private class UsableOperators{
        
        private Point m_Field;
        private ArrayList<Operator> m_Operators;

        public UsableOperators() {
            this(new Point(), new ArrayList<>());
        }

        public UsableOperators(Point _Field, ArrayList<Operator> _UsableOperators){
            if (_Field == null){
                //throw exception?? - throw exception!!
                throw new NullPointerException("Parameter _Field can't be null in UsableOperators!");
            }
            
            this.m_Field = _Field;

            if (_UsableOperators == null){
                this.m_Operators = new ArrayList<>();
            }
            else{
                this.m_Operators = _UsableOperators;
            }
        }

        public UsableOperators(Point _Field, Operator _Operator) {
            if (_Field == null){
                //throw exception?? - throw exception!!
                throw new NullPointerException("Parameter _Field can't be null in UsableOperators!");
            }
            else{
                this.m_Field = _Field;
            }
            
            ArrayList<Operator> operators = new ArrayList<>();
            if (_Operator != null){
                operators.add(_Operator);
            }

            this.m_Operators = operators;
        }

        public Point getField(){
            return m_Field;
        }

        public void setField(Point _Field){
            if (_Field == null){
                //throw exception?? - throw exception!!
                throw new NullPointerException("Parameter _Field can't be null in UsableOperators!");
            }
            
            m_Field = _Field;
        }

        public ArrayList<Operator> getOperators(){
            return m_Operators;
        }

        public void setOperators(ArrayList<Operator> _Operators){
            if (_Operators == null){
                m_Operators.clear();
            }
            else{
                m_Operators = _Operators;
            }
        }

        public void addOperator(Operator _Operator){
            if (_Operator != null){
                if (m_Operators == null){
                    m_Operators = new ArrayList<>();
                }
                
                m_Operators.add(_Operator);
            }
        }

        public boolean isOperatorUsable(Operator _Operator){
            if (m_Operators != null && _Operator != null){
                return m_Operators.contains(_Operator);
            }
            else{
                return false;
            }
        }
    }

    private HashMap<Point, UsableOperators> m_List;

    public FieldOperators() {
        this.m_List = new HashMap<>();
    }
    
    public void addOperators(Point _Field, ArrayList<Operator> _Operators){
        if (_Field == null){
            //throw exception?? - throw exception!!
            throw new NullPointerException("Parameter _Field can't be null in FieldOperators!");
        }
        
        if (_Operators == null){
            _Operators = new ArrayList<>();
        }
        
        if (m_List == null){
            m_List = new HashMap<>();
        }

        if (! m_List.containsKey(_Field)){
            m_List.put(_Field, new UsableOperators(_Field, _Operators));
        }
        else{
            m_List.replace(_Field, new UsableOperators(_Field, _Operators));
        }
    }
    
    public void addOperator(Point _Field, Operator _Operator){
        if (_Field != null && _Operator != null){
            if (m_List == null){
                m_List = new HashMap<>();
            }
            
            if (! m_List.containsKey(_Field)){
                m_List.put(_Field, new UsableOperators(_Field, _Operator));
            }
            else{
                UsableOperators Operators = m_List.get(_Field);
            }
        }
    }
    
    public ArrayList<Operator> getOperators(int _X, int _Y){
        Point field;
        field = new Point(_X, _Y);
        
        return m_List.get(field).getOperators();
    }
}
