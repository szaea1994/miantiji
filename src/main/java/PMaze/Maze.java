/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PMaze;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonArray;
import IPFileActions.ISave;
import IPFileActions.ILoad;

/**
 * <p>
 * This class is the responsible for creating and handling labyrinths. With this
 * class you are able to create a random labyrinth with the sizes given by you.
 *
 * <p>
 * You are also able to save and load the labyrinth into JSON, because of the
 * implementation of {@link IPFileActions.ISave} and {@link IPFileActions.ILoad}
 * interfaces.
 *
 * <p>
 * The used algorithm will generate a labyrinth which every part can be reached
 * from every field of it. There are no separeted parts.
 *
 * @author Szentpéteri Annamária
 */
public class Maze implements ISave, ILoad {
/* ---- CLASS VARIABLES ---- */
    private static Maze m_Instance = null;

    
    /**
     * Height of the labyrinth.
     */
    private int m_Height;
    /**
     * Width of the labyrinth.
     */
    private int m_Width;
    /**
     * Array which stores the horizontal positioned walls.
     */
    private ArrayList<BitSet> m_HorizontalWalls;
    /**
     * Array which stores the vertical positioned walls.
     */
    private ArrayList<BitSet> m_VerticalWalls;
    /**
     * If true, the generating algorithm will include making alternate routes
     * in the maze by deleting borders of random fields.
     */
    private boolean m_NeedAlternateRoutes;
    
    private boolean m_Empty;


/* ---- CONTSTRUCTORS ---- */
    /**
     * Creates an initial, empty labyrinth with the size 2x2.
     */
    private Maze() {
        this.m_Width = 2;
        this.m_Height = 2;
        
        this.m_NeedAlternateRoutes = false;
        
        init();
        empty();
    }


/* ---- GETTER METHODS ---- */
    public static Maze getInstance(){
        if (m_Instance == null){
            m_Instance = new Maze();
        }
        return m_Instance;
    }
    
    /**
     * Returns the m_Height of the labyrinth.
     *
     * @return m_Height of the labyrinth
     */
    public int getHeight() {
        return m_Height;
    }

    /**
     * Returns the m_Width of the labyrinth.
     *
     * @return m_Width of the labyrinth
     */
    public int getWidth() {
        return m_Width;
    }

    /**
     * Gets coordinates of a field, returns the field's border information.
     *
     * @param _X horizontal coordinate of the field
     * @param _Y vertical coordinate of the field
     * @return a [top, left, bottom, right] array which members are either true
     * or false
     */
    public ArrayList<Boolean> getFieldBorders(int _X, int _Y) {
        ArrayList<Boolean> result;

        try {
            result = new ArrayList<>();

            // Add north border.
            result.add(m_HorizontalWalls.get(_Y).get(_X));
            // Add west border.
            result.add(m_VerticalWalls.get(_X).get(_Y));
            // Add south border.
            result.add(m_HorizontalWalls.get(_Y + 1).get(_X));
            // Add east border.
            result.add(m_VerticalWalls.get(_X + 1).get(_Y));

        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @return
     */
    public boolean getFieldNorthBorder(int _X, int _Y) {
        return m_HorizontalWalls.get(_Y).get(_X);
    }

    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @return
     */
    public boolean getFieldEastBorder(int _X, int _Y) {
        return m_VerticalWalls.get(_X + 1).get(_Y);
    }

    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @return
     */
    public boolean getFieldSouthBorder(int _X, int _Y) {
        return m_HorizontalWalls.get(_Y + 1).get(_X);
    }

    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @return
     */
    public boolean getFieldWestBorder(int _X ,int _Y) {
        return m_VerticalWalls.get(_X).get(_Y);
    }
    
    public boolean getNeedAlternateRoutes(){
        return this.m_NeedAlternateRoutes;
    }
    
    public boolean getIsEmpty(){
        return m_Empty;
    }


/* ---- SETTER METHODS ---- */ 
    /**
     * Sets the m_Width of the labyrinth.
     *
     * @param _Width the given m_Width
     * @param _Height
     */
    public void setSize(int _Width, int _Height) {
        if (_Height > 1) {
            m_Height = _Height;
        }
        if (_Width > 1) {
            m_Width = _Width;
        }
        
        init();
        empty();
    }
    
    public void setNeedAlternateRoutes(boolean _Need){
        this.m_NeedAlternateRoutes = _Need;
    }

    /**
     * Sets the field's borders by the given information.
     *
     * Gets coordinates of a field and an array with information about the
     * borders in this strict order: top,left,bottom,right
     *
     * @param _X horizontal coordinate of the field
     * @param _Y vertical coordinate of the field
     * @param _Borders a [top, left, bottom, right] array which contains
     * information about the field's borders
     */
    public void setFieldBorders(int _X, int _Y, ArrayList<Boolean> _Borders) {
        if ((_X >= 0) && (_Y >= 0) && (_X < m_Width) && (_Y < m_Height)) {
            // Set top border.
            m_HorizontalWalls.get(_Y).set(_X, _Borders.get(0));
            // Set left border.
            m_VerticalWalls.get(_X).set(_Y, _Borders.get(1));
            // Set bottom border.
            m_HorizontalWalls.get(_Y + 1).set(_X, _Borders.get(2));
            // Set right border.
            m_VerticalWalls.get(_X + 1).set(_Y, _Borders.get(3));
        } else {
            System.out.println("setFieldBorders - Indexes out of border. Set nothing.");
        }
    }
    
    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @param _HasBorder
     */
    public void setFieldNorthBorder(int _X, int _Y, boolean _HasBorder) {
        m_HorizontalWalls.get(_Y).set(_X, _HasBorder);
    }

    /**
     * TODO
     * 
     * @param _X
     * @param _Y
     * @param _HasBorder
     */
    public void setFieldEastBorder(int _X, int _Y, boolean _HasBorder) {
        m_VerticalWalls.get(_X + 1).set(_Y, _HasBorder);
    }

    /**
     *
     * @param _X
     * @param _Y
     * @param _HasBorder
     */
    public void setFieldSouthBorder(int _X, int _Y, boolean _HasBorder) {
        m_HorizontalWalls.get(_Y + 1).set(_X, _HasBorder);
    }

    /**
     *
     * @param _X
     * @param _Y
     * @param _HasBorder
     */
    public void setFieldWestBorder(int _X, int _Y, boolean _HasBorder) {
        m_VerticalWalls.get(_X).set(_Y, _HasBorder);
    }


/* ---- OTHER METHODS ---- */ 
    /**
     * <p>
     * Initialize the labyrinth.
     *
     * <p>
     * It's a completely raw structure, no wall information will be set here.
     *
     * <p>
     * This method is only suitable to set the correct sizes.
     */
    public final void init() {
        if (m_HorizontalWalls != null){
            m_HorizontalWalls.clear();
        }
        if (m_VerticalWalls != null){
            m_VerticalWalls.clear();
        }
        
        m_HorizontalWalls = new ArrayList<>();
        m_VerticalWalls = new ArrayList<>();

        for (int i = 0; i < m_Height + 1; i++) {
            m_HorizontalWalls.add(new BitSet(m_Width));
        }
        for (int i = 0; i < m_Width + 1; i++) {
            m_VerticalWalls.add(new BitSet(m_Height));
        }
    }

    /**
     * <p>
     * Clears the labyrinth.
     *
     * <p>
     * Creates the borders of the labyrinth and deletes the inner walls making a
     * clean "room".
     */
    public final void empty() {
        // Deleting all horizontal walls.
        for (BitSet bs : m_HorizontalWalls) {
            bs.clear();
        }

        // Deleting all vertical walls.
        for (BitSet bs : m_VerticalWalls) {
            bs.clear();
        }

        // Borders of the labyrinth.
        int bsLength;

        // Top side of the labyrinth.
        bsLength = m_HorizontalWalls.get(0).size();
        m_HorizontalWalls.get(0).flip(0, bsLength);

        // Bottom side of the labyrinth.
        bsLength = m_HorizontalWalls.get(m_HorizontalWalls.size() - 1).size();
        m_HorizontalWalls.get(m_HorizontalWalls.size() - 1).flip(0, bsLength);

        // Left side of the labyrinth.
        bsLength = m_VerticalWalls.get(0).size();
        m_VerticalWalls.get(0).flip(0, bsLength);

        // Right side of the labyrinth.
        bsLength = m_VerticalWalls.get(m_VerticalWalls.size() - 1).size();
        m_VerticalWalls.get(m_VerticalWalls.size() - 1).flip(0, bsLength);
        
        m_Empty = true;
    }

    /**
     * <p>
     * Generates a random labyrinth.
     *
     * <p>
     * First, it empties the labyrinth, then makes a new one by random values.
     */
    public void generate() {
        // First needs a clean up the labyrinth.
        if (!m_Empty){
            empty();
        }

        // Random number generator to randomize the generating method.
        Random rand = new Random();

        // Vertical or horizontal walls will be generated depending on this variable.
        int hORv;
        
        // Stores that which line will be filled with walls.
        int line;

        // Arrays to handle which lines were filled and which wasn't.
        ArrayList<Integer> hIndexes;
        hIndexes = new ArrayList<>();
        ArrayList<Integer> vIndexes;
        vIndexes = new ArrayList<>();

        /* Filling the arrays with line numbers.
         * The first and last element will never be needed because of the init()
         * function, which already filled them. */
        for (int i = 1; i < m_HorizontalWalls.size() - 1; i++) {
            hIndexes.add(i);
        }
        for (int i = 1; i < m_VerticalWalls.size() - 1; i++) {
            vIndexes.add(i);
        }

        // While both arrays have unfilled lines, decide randomly which side and line will filled.
        while (!(hIndexes.isEmpty() && vIndexes.isEmpty())) {
            // If one of the arrays is empty, it's fixed that which side needs to be chosen. 
            if (hIndexes.isEmpty()) {
                hORv = rand.nextInt(2) + 2;
            }
            else if (vIndexes.isEmpty()) {
                hORv = rand.nextInt(2);
            }
            else{
                hORv = rand.nextInt(4);
            }

            /* If hORv is 0 or 1 then generates a horizontal line. If it is 2 or
             * 3 it's generates a vertical line. The number of line is choosed
             * by random numbers too.
             *
             * The generating method is:
             *  - check if perpendicularly have a border "behind"/"above" or "before"/"under" the current line
             *  - yes: don't put wall there
             *  - no: put wall there
             *
             *  behind/above: hORv = 0 or 2 in the for cycle
             *  before/under: hORv = 1 or 3 in the for cycle */
            if ((hORv == 0) || (hORv == 1)) {
                // Get line number and remove from the list
                line = hIndexes.remove(rand.nextInt(hIndexes.size()));

                // Generating
                for (int i = 0; i < m_Width; i++) {
                    if (m_VerticalWalls.get(i + hORv).get(line - 1) || m_VerticalWalls.get(i + hORv).get(line)) {
                        m_HorizontalWalls.get(line).set(i, false);
                    } else {
                        m_HorizontalWalls.get(line).set(i, true);
                    }
                }
            } else {
                hORv -= 2;

                // Get line number and remove from the list
                line = vIndexes.remove(rand.nextInt(vIndexes.size()));

                // Generating
                for (int i = 0; i < m_Height; i++) {
                    if (m_HorizontalWalls.get(i + hORv).get(line - 1) || m_HorizontalWalls.get(i + hORv).get(line)) {
                        m_VerticalWalls.get(line).set(i, false);
                    } else {
                        m_VerticalWalls.get(line).set(i, true);
                    }
                }
            }
        }
        
        //If true, the algorithm empties the borders of a few fields, making alternate routes by that
        if (m_NeedAlternateRoutes){
            //For the rating formula many thanks to Bereczki László!
            int i = (int) Math.round( ((m_Height * m_Width)/100.0) * 15.0 ); 
            //Thank you man!
            
            for(; i > 0; i--){
                int x = rand.nextInt(m_Height);
                int y = rand.nextInt(m_Width);
                
                if (x > 0)
                    setFieldWestBorder(x, y, false);
                if (x < m_Width - 1)
                    setFieldEastBorder(x, y, false);
                if (y > 0)
                    setFieldNorthBorder(x, y, false);
                if (y < m_Height - 1)
                    setFieldSouthBorder(x, y, false);
            }
        }
        
        m_Empty = false;
    }

    
/* ---- FILEACTION INTERFACE METHODS ---- */
    /**
     * Implementation of {@link IPFileActions.ILoad} interface.
     *
     * @return return value is true if the loading process was successfull
     * @see fileactions.ILoad#LoadFromJSON()
     */
    @Override
    public Boolean loadFromJSON() {
        return loadFromJSON(ILoad.FILENAME);
    }

    /**
     * Implementation of {@link IPFileActions.ISave} interface.
     *
     * @return return value is true if the saving process was successfull
     * @see fileactions.ISave#SaveToJSON()
     */
    @Override
    public Boolean saveToJSON() {
        return saveToJSON(ISave.FILENAME);
    }

    /**
     * Implementation of {@link IPFileActions.ILoad} interface.
     *
     * @param _FileName name of the file which will be processed
     * @return return value is true if the loading process was successfull
     * @see fileactions.ILoad#LoadFromJSON(java.lang.String)
     */
    @Override
    public Boolean loadFromJSON(String _FileName) {
        boolean done;
        done = false;

        try (InputStream input = new FileInputStream(_FileName);) {
            try (Reader in = new BufferedReader(new InputStreamReader(input))) {
                JsonObject jo = JsonObject.readFrom(in);
                
                this.setSize(jo.get("width").asInt(), jo.get("height").asInt());
                
                this.init();
                
                JsonArray horizontal = jo.get("horizontal").asArray();
                JsonArray vertical = jo.get("vertical").asArray();
                
                for (Integer i = 0; i < (m_Height + 1); i++) {
                    String value = horizontal.get(i).asObject().get(i.toString()).asString();
                    
                    for (int j = 0; j < m_Width; j++) {
                        if (value.charAt(j) == '1') {
                            m_HorizontalWalls.get(i).set(j);
                        } else {
                            m_HorizontalWalls.get(i).clear(j);
                        }
                    }
                }
                
                for (Integer i = 0; i < (m_Width + 1); i++) {
                    String value = vertical.get(i).asObject().get(i.toString()).asString();
                    
                    for (int j = 0; j < m_Height; j++) {
                        if (value.charAt(j) == '1') {
                            m_VerticalWalls.get(i).set(j);
                        } else {
                            m_VerticalWalls.get(i).clear(j);
                        }
                    }
                }
                
                done = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        m_Empty = false;
        
        return done;
    }

    /**
     * Implementation of {@link IPFileActions.ISave} interface.
     *
     * @param _FileName name of the file which will be processed
     * @return return value is true if the saving process was successfull
     * @see fileactions.ISave#SaveToJSON(java.lang.String)
     */
    @Override
    public Boolean saveToJSON(String _FileName) {
        boolean done;
        done = false;

        try (OutputStream output = new FileOutputStream(_FileName);) {
            try (Writer out = new BufferedWriter(new OutputStreamWriter(output))) {
                JsonObject jo = new JsonObject();
                
                jo.add("width", m_Width);
                jo.add("height", m_Height);
                
                JsonArray horizontal = new JsonArray();
                JsonArray vertical = new JsonArray();
                
                for (Integer i = 0; i < m_HorizontalWalls.size(); i++) {
                    String id = i.toString();
                    String value = "";
                    
                    for (int j = 0; j < m_Width; j++) {
                        if (m_HorizontalWalls.get(i).get(j)) {
                            value = value.concat("1");
                        } else {
                            value = value.concat("0");
                        }
                    }
                    
                    horizontal.add(new JsonObject().add(id, value));
                }
                
                for (Integer i = 0; i < m_VerticalWalls.size(); i++) {
                    String id = i.toString();
                    String value = "";
                    
                    for (int j = 0; j < m_Height; j++) {
                        if (m_VerticalWalls.get(i).get(j)) {
                            value = value.concat("1");
                        } else {
                            value = value.concat("0");
                        }
                    }
                    
                    vertical.add(new JsonObject().add(id, value));
                }
                
                jo.add("horizontal", horizontal);
                jo.add("vertical", vertical);
                
                jo.writeTo(out);
                
                done = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return done;
    }
}
