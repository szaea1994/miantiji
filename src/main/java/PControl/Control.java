/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the destroy Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PControl;

import IPFileActions.ILoad;
import IPFileActions.ISave;
import PAgent.ObjectAgent;
import PAgent.MIAgent;
import PAgent.Agent;
import PCommon.Types;
import PCommon.Types.Algorithm;
import PCommon.Types.Heuristic;
import PCommon.Types.Operator;
import PMaze.Maze;
import PUI.PositionSettingDlg;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Contorls the game flow.
 * 
 * @author Szentpéteri Annamária
 */
public class Control implements ILoad, ISave {
/* ---- CLASS VARIABLES ---- */
    public static Control m_Instance = null;
    
    
    protected class StoreAgent{
        private Agent m_Agent = null;
        private Point m_Position = null;
        
        StoreAgent(){
        }
        
        StoreAgent(Agent _Agent, Point _Position){
            m_Agent = _Agent;
            m_Position = _Position;
        }
        
        public Agent getAgent(){
            return m_Agent;
        }
        
        public Point getPosition(){
            return m_Position;
        }
        
        public void setAgent(Agent _Agent){
            m_Agent = _Agent;
        }
        
        public void setPosition(Point _Position){
            m_Position = _Position;
        }
    }  
    
    protected StoreAgent m_Agent = null;
    protected List<StoreAgent> m_Objects = new ArrayList<>();
    protected Point m_Exit = new Point(0,0);
    protected int m_Sight;
    protected boolean m_IsGameOver;
    protected boolean m_IsClear;

    
/* ---- CONTSTRUCTORS ---- */
    /**
     * todo
     */
    protected Control() { 
        //create a simple default game
        clear();
    }

    
    
/* ---- GETTER METHODS ---- */   
    public static Control getInstance(){
        if (m_Instance == null){
            m_Instance = new Control();
        }
        return m_Instance;
    }
    
    /**
     *
     * @return
     */
    public int getNumberOfObjects(){
        return m_Objects.size();
    }

    /**
     *
     * @return
     */
    public ArrayList<Point> getPositionOfObjects(){
        ArrayList<Point> result = new ArrayList<>();
        
        for (StoreAgent sa: m_Objects){
            result.add(sa.getPosition());
        }
        
        return result;
    }

    /**
     *
     * @return
     */
    public Point getPositionOfAgent(){
        if (m_Agent == null){
            return null;
        }
        else{
            return m_Agent.getPosition();
        }
    }
    
    /**
     *
     * @return
     */    
    public int getSight(){
        return m_Sight;
    }

    public HashSet<Point> getAgentSeenFields() {
        if (m_Agent == null){
            return null;
        }
        else{
            MIAgent agent = (MIAgent)(m_Agent.getAgent());
            if (agent == null){
                return null;
            }
            else{
                return agent.getSeenFields();
            }
        }   
    }

    public HashSet<Point> getAgentSeenObjects() {
        if (m_Agent == null){
            return null;
        }
        else{
            MIAgent agent = (MIAgent)(m_Agent.getAgent());
            if (agent == null){
                return null;
            }
            else{
                return agent.getSeenObjects();
            }
        }
        
    }
    
    protected HashSet<Point> getVisibleFields(Point _Position){
        //todo
        //a maze és a sight alapján visszaadja a látható mezők koordinátáit
        HashSet<Point> result = null;
        
        if (_Position != null){
            result = new HashSet<>();
            result.add(_Position);
        
            //mind a 4 irányba sight-szor:
            Point posNorth = new Point(_Position);
            Point posEast = new Point(_Position);
            Point posSouth = new Point(_Position);
            Point posWest = new Point(_Position);

            for(int i = 0; i < m_Sight; i++){
                //north
                if (!Maze.getInstance().getFieldNorthBorder(posNorth.x, posNorth.y)){
                    posNorth.setLocation(posNorth.x, posNorth.y - 1);
                    result.add(new Point(posNorth));
                }

                //east
                if (!Maze.getInstance().getFieldEastBorder(posEast.x, posEast.y)){
                    posEast.setLocation(posEast.x + 1, posEast.y);
                    result.add(new Point(posEast));
                }

                //south
                if (!Maze.getInstance().getFieldSouthBorder(posSouth.x, posSouth.y)){
                    posSouth.setLocation(posSouth.x, posSouth.y + 1);
                    result.add(new Point(posSouth));
                }

                //west
                if (!Maze.getInstance().getFieldWestBorder(posWest.x, posWest.y)){
                    posWest.setLocation(posWest.x - 1, posWest.y);
                    result.add(new Point(posWest));
                }
            }
        }
        
        return result;
    }
    
    protected HashSet<Point> getVisibleObjects(HashSet<Point> _VisibleFields){
        //todo
        //visiblefields koordináták alapján visszaadja a látható objektumok koordinátáit
        HashSet<Point> result = null;
        
        if (!m_Objects.isEmpty() && _VisibleFields != null){
            for(StoreAgent sa: m_Objects){
                if (sa != null){
                    Point posObject = sa.getPosition();

                    for(Point vf: _VisibleFields){
                        if (vf != null && vf.equals(posObject)){
                            if (result == null){
                                result = new HashSet<>();
                            }
                            result.add(posObject);
                            break;
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    public Point getExit(){
        return m_Exit;
    }
    
/* ---- SETTER METHODS ---- */ 
    /**
     * todo
     * @param _Sight
     */    
    public void setSight(int _Sight){
        m_Sight = _Sight;
    }
    
/* ---- OTHER METHODS ---- */     
    /**
     * todo
     * @param _Sight
     * @param _NumOfObj
     * @param _Heuristic
     * @param _Algorithm
     */
    public void init(int _Sight, int _NumOfObj, Heuristic _Heuristic, Algorithm _Algorithm){
        clear();

        int width = Maze.getInstance().getWidth();
        int height = Maze.getInstance().getHeight();

        m_Sight = _Sight;
        m_Exit.setLocation(width - 1, height - 1);

        //objects
        Point position;
        PositionSettingDlg dialog = new PositionSettingDlg(new javax.swing.JFrame(), true, width, height);

        for(int i = 0; i < _NumOfObj; i++){
            position = dialog.ShowDialog();
            m_Objects.add(new StoreAgent(new ObjectAgent(), position));
        }

        //ágens
        m_Agent.setPosition(new Point(0,0));
        MIAgent agent = (MIAgent)(m_Agent.getAgent());

        agent.setFieldHeuristic(width, height, _Heuristic);
        agent.setAlgorithm(_Algorithm);
        agent.setDepth(5);
        HashSet<Point> visibleFields = getVisibleFields(new Point(0,0));
        agent.addToSeenFields(visibleFields);
        agent.addToSeenObjects(getVisibleObjects(visibleFields));

        m_IsClear = false;

        // lehet már ebben a helyzetben is végállapot, ezért ellenőrizni kell
        // (pl.: objektum van a kijáraton, objektum van az ágens mezőjén)
        CheckGameOver();
    }
    
    public void clear(){
        //értékek alaphelyzetbe állítása
        m_IsGameOver = false;
        m_IsClear = true;
        
        m_Agent = new StoreAgent(new MIAgent(), new Point(0,0));
        m_Objects.clear();
        
        m_Exit.setLocation(0, 0);
        m_Sight = 0;
    }
    
    /**
     *
     * @return
     */
    public boolean isGameOver(){
        return m_IsGameOver;
    }
    
    /**
     * 
     */
    public void nextStep(){
        if (m_IsGameOver){
            return;
        }
        
        boolean valid = false;
        
        if (m_Agent != null){
            MIAgent agent = (MIAgent)(m_Agent.getAgent());
             
            if (agent != null){
                Point currPosition = m_Agent.getPosition();
                HashSet<Point> currVisibleFields = getVisibleFields(currPosition);
                HashSet<Point> currVisibleObj = getVisibleObjects(currVisibleFields);
                Point newPosition = null;
                
                while (!valid){
                    newPosition = agent.yourTurn(currPosition, currVisibleFields, currVisibleObj, newPosition);

                    if (isStepValid(currPosition, newPosition)){
                        if (!m_IsGameOver){
                            m_Agent.setPosition(newPosition);

                            HashSet<Point> newVisibleFields = getVisibleFields(newPosition);
                            agent.addToSeenFields(newVisibleFields);
                            agent.addToSeenObjects(getVisibleObjects(newVisibleFields));
                        }
                        
                        valid = true;
                    }
                }
            }
        }
        
        if (!m_Objects.isEmpty()){
            for(StoreAgent sa: m_Objects){
                valid = false;
                ObjectAgent agent = (ObjectAgent)(sa.getAgent());
                Point currPosition = sa.getPosition(); 
                
                while (!valid){
                    if (agent != null){
                        Point newPosition = agent.yourTurn(currPosition);
                        
                        if (isStepValid(currPosition, newPosition)){
                            sa.setPosition(newPosition);
                            valid = true;
                        }
                    }
                    else{
                        valid = true;
                    }
                }
            }
        }
        
        CheckGameOver();
    }
    
    public boolean isClear(){
        return m_IsClear;
    }
    
    protected boolean isStepValid(Point _Current, Point _New){
        boolean result = false;
        
        //egyhelyben marad
        if (_Current.equals(_New)){
            result = true;
        }
        else{
            //feladja; (-1,-1) koordinátát adott vissza
            if (Types.GIVE_UP.equals(_New)){
                result = true;
                m_IsGameOver = true;
            }
            else{
                //az aktuális helyről elérhető mezőkre lépett-e
                Point validField = new Point();
                if (!Maze.getInstance().getFieldNorthBorder(_Current.x, _Current.y)){
                    validField.setLocation(_Current.x, _Current.y - 1);
                
                    if (validField.equals(_New)){
                        result = true;
                    }
                }
                if (!Maze.getInstance().getFieldEastBorder(_Current.x, _Current.y)){
                    validField.setLocation(_Current.x + 1, _Current.y);
                
                    if (validField.equals(_New)){
                        result = true;
                    }
                    
                }
                if (!Maze.getInstance().getFieldSouthBorder(_Current.x, _Current.y)){
                    validField.setLocation(_Current.x, _Current.y + 1);
                
                    if (validField.equals(_New)){
                        result = true;
                    }
                    
                }
                if (!Maze.getInstance().getFieldWestBorder(_Current.x, _Current.y)){
                    validField.setLocation(_Current.x - 1, _Current.y);
                
                    if (validField.equals(_New)){
                        result = true;
                    }
                    
                }
            }
        }
        
        return result;
    }
    
    protected void CheckGameOver(){
        //ellenőrzések hogy vég állapotba került-e a játék        
        if (!m_IsGameOver){
            Point posAgent;
            
            //ágens
            if (m_Agent != null){
                posAgent = m_Agent.getPosition();
                
                if (posAgent == null){
                    m_IsGameOver = true;
                }
                else if (m_Exit.equals(posAgent)){
                    m_IsGameOver = true;
                }
                else if (!m_Objects.isEmpty()){
                    Point posObj;

                    for(StoreAgent sa: m_Objects){
                        posObj = sa.getPosition();

                        if (posAgent.equals(posObj)){
                            m_IsGameOver = true;
                            break;
                        }                            
                        if (m_Exit.equals(posObj)){
                            m_IsGameOver = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    
/* ---- FILEACTION INTERFACE METHODS ---- */

    @Override
    public Boolean loadFromJSON() {
        throw new UnsupportedOperationException("TODO XXX"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean loadFromJSON(String _FileName) {
        throw new UnsupportedOperationException("TODO XXX"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean saveToJSON() {
        throw new UnsupportedOperationException("TODO XXX"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean saveToJSON(String _FileName) {
        throw new UnsupportedOperationException("TODO XXX"); //To change body of generated methods, choose Tools | Templates.
    }

    
}
