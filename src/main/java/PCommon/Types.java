/*
 * Copyright (C) 2016 User
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PCommon;

import java.awt.Point;

/**
 *
 * @author User
 */
public class Types {

    /**
     * todo
     */
    public enum Heuristic{
        
        /**
         *
         */
        DEFAULT,

        /**
         *
         */
        DISTANCE,

        /**
         *
         */
        OBJECT,

        /**
         *
         */
        DISTANCE_OBJECT
    }

    /**
     * todo
     */
    public enum Algorithm{

        /**
         *
         */
        CURRENT_POSITION,

        /**
         *
         */
        CURRENT_SEEN,

        /**
         *
         */
        ALL_SEEN
    }
    
    /**
     * todo
     */
    public enum GameState {

        /**
         *
         */
        DEFAULT,

        /**
         *
         */
        HAS_MAZE,

        /**
         *
         */
        HAS_GAME,

        /**
         *
         */
        IN_PROGRESS
    }
    
    /**
     * todo
     */
    public enum Operator{

        /**
         *
         */
        UP,

        /**
         *
         */
        DOWN,

        /**
         *
         */
        RIGHT,

        /**
         *
         */
        LEFT,

        /**
         *
         */
        GIVE_UP
    }
    
    public static Point GIVE_UP = new Point(-1, -1);
}
