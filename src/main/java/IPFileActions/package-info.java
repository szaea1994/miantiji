/**
 * Basic interfaces to save and load labyrinth and gamestate
 * from a json file.
 * 
 * @author Szentpéteri Annamária
 *
 */
package IPFileActions;