/*
 * Copyright (C) 2016 Szentpéteri Annamária
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package IPFileActions;

/**
 * @author Szentpéteri Annamária
 * 
 * <p>Interface which gives the methods
 * to load your class from a file.
 */
public interface ILoad {
	/** The default input name. */
	String FILENAME = "./default.json";

	/**
	 * Loads information from the default
	 * file.
	 * 
	 * @return return value is true if the loading
	 *         process was successfull
	 */
	Boolean loadFromJSON();
	
	/**
	 * Loads information from the given
	 * file.
	 * 
	 * @param _FileName name of the file which will be
	 *                 processed
	 * @return return value is true if the loading
	 *         process was successfull
	 */
	Boolean loadFromJSON(String _FileName);
}
