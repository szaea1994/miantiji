/*
 * Copyright (C) 2016 User
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package PUI;

import PCommon.Types.Algorithm;
import PControl.Control;
import PMaze.Maze;
import PCommon.Types.GameState;
import PCommon.Types.Heuristic;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 *
 * @author User
 */
public class Main extends javax.swing.JFrame {
    
/* ---- NON-COMPONENT VARIABLES ---- */
    private GameState m_State;
    private boolean m_AutoRunning;
    private RunWorker m_RunWorker;

    
/* ---- INNER CLASSES ---- */    
    private class GamePanel extends JPanel {
        private int m_FieldWidth;
        private int m_FieldHeight;
        private boolean m_DrawUnseenObjects;
        private Control m_Control;
        
        private int ORIGINAL_HEIGHT;
        private int ORIGINAL_WIDTH;
        private final int MARGO;
        
        public GamePanel(){
            super();
            
            m_FieldWidth = 0;
            m_FieldHeight = 0;
            
            ORIGINAL_HEIGHT = 0;
            ORIGINAL_WIDTH = 0;
            MARGO = 2;
            
            m_DrawUnseenObjects = true;
            m_Control = null;
        }
        
        public void saveSize(){
            ORIGINAL_HEIGHT = this.getHeight();
            ORIGINAL_WIDTH = this.getWidth();
        }
        
        public void setMaze(){
            if (Maze.getInstance().getIsEmpty()){
                m_FieldWidth = 0;
                m_FieldHeight = 0;

                this.setSize(ORIGINAL_WIDTH, ORIGINAL_HEIGHT);
            }
            else{
                m_FieldWidth = ORIGINAL_WIDTH / Maze.getInstance().getWidth();
                m_FieldHeight = ORIGINAL_HEIGHT / Maze.getInstance().getHeight();

                int width = ORIGINAL_WIDTH;
                int height = ORIGINAL_HEIGHT;

                if ((ORIGINAL_WIDTH % Maze.getInstance().getWidth()) != 0){
                    width = Maze.getInstance().getWidth() * m_FieldWidth;
                }
                if ((ORIGINAL_HEIGHT % Maze.getInstance().getHeight()) != 0){
                    height = Maze.getInstance().getHeight() * m_FieldHeight;
                }

                this.setSize(width, height);
            }
        }
                
        public void redraw(Control _Control, boolean _DrawUnseenObjects){
            m_DrawUnseenObjects = _DrawUnseenObjects;
            m_Control = _Control;
            setMaze();            
            repaint();
        }
        
        public void paintMaze(Graphics _Graph){
            for (int y = 0; y < Maze.getInstance().getHeight(); y++){
                int fieldY = y * m_FieldHeight;
                
                for (int x = 0; x < Maze.getInstance().getWidth(); x++){
                    int fieldX = x * m_FieldWidth;
                    
                    //Drawing grid of the maze
                    _Graph.setColor( new Color(240, 240, 240) );
                    _Graph.drawLine(fieldX, fieldY, fieldX + m_FieldWidth, fieldY); 
                    _Graph.drawLine(fieldX + m_FieldWidth, fieldY, fieldX + m_FieldWidth, fieldY + m_FieldHeight);
                    _Graph.drawLine(fieldX, fieldY + m_FieldHeight, fieldX + m_FieldWidth, fieldY + m_FieldHeight);
                    _Graph.drawLine(fieldX, fieldY, fieldX, fieldY + m_FieldHeight);
                    
                    //Drawing the walls
                    _Graph.setColor(Color.BLACK);
                    if (Maze.getInstance().getFieldNorthBorder(x, y)){
                       _Graph.drawLine(fieldX, fieldY, fieldX + m_FieldWidth, fieldY); 
                    }
                    if (Maze.getInstance().getFieldEastBorder(x, y)){
                        _Graph.drawLine(fieldX + m_FieldWidth, fieldY, fieldX + m_FieldWidth, fieldY + m_FieldHeight);
                    }
                    if (Maze.getInstance().getFieldSouthBorder(x, y)){
                        _Graph.drawLine(fieldX, fieldY + m_FieldHeight, fieldX + m_FieldWidth, fieldY + m_FieldHeight);
                    }
                    if (Maze.getInstance().getFieldWestBorder(x, y)){
                        _Graph.drawLine(fieldX, fieldY, fieldX, fieldY + m_FieldHeight);
                    }
                }
            }
        }
        
        public void paintAgent(Graphics _Graph, int x, int y){
            _Graph.fillOval(x * m_FieldWidth + MARGO,
                    y * m_FieldHeight + MARGO,
                    m_FieldWidth - 2*MARGO,
                    m_FieldHeight - 2*MARGO);
        }
        
        public void paintObject(Graphics _Graph, int x, int y){
            _Graph.drawOval(x * m_FieldWidth + 2*MARGO,
                    y * m_FieldHeight + 2*MARGO,
                    m_FieldWidth - 2*2*MARGO,
                    m_FieldHeight - 2*2*MARGO);
            _Graph.drawLine(x * m_FieldWidth + 2*MARGO,
                    y * m_FieldHeight + 2*MARGO,
                    x * m_FieldWidth + 2*MARGO + m_FieldWidth - 2*2*MARGO ,
                    y * m_FieldHeight + 2*MARGO + m_FieldHeight - 2*2*MARGO);
            _Graph.drawLine(x * m_FieldWidth + 2*MARGO + m_FieldWidth - 2*2*MARGO,
                    y * m_FieldHeight + 2*MARGO,
                    x * m_FieldWidth + 2*MARGO,
                    y * m_FieldHeight + 2*MARGO + m_FieldHeight - 2*2*MARGO);
        }
        
        public void paintSeen(Graphics _Graph, int x, int y){
            _Graph.fillRect(x * m_FieldWidth + MARGO, y * m_FieldHeight + MARGO, m_FieldWidth - 2*MARGO, m_FieldHeight - 2*MARGO);
        }
        
        public void paintExit(Graphics _Graph, int x, int y){
            _Graph.fillRect(x * m_FieldWidth + MARGO, y * m_FieldHeight + MARGO, m_FieldWidth - 2*MARGO, m_FieldHeight - 2*MARGO);
        }
        
        public void paintGameOver(Graphics _Graph){
            _Graph.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 72));
            _Graph.drawString("GAME OVER", 25, 200);
        }
        
        @Override
        public void paintComponent(Graphics _Graph){
            super.paintComponent(_Graph);
            
            //draw labirynth
            if (Maze.getInstance() != null){
                paintMaze(_Graph);
            }
            
            if (m_Control != null && !m_Control.isClear()){
                //draw seen fields by agent
                _Graph.setColor(Color.YELLOW);
                HashSet<Point> points = m_Control.getAgentSeenFields();
                if (points != null){
                    for (Point seen: points){
                        paintSeen(_Graph, seen.x, seen.y);
                    }
                }
                //draw exit
                _Graph.setColor(Color.GRAY);
                Point exit = m_Control.getExit();
                if (exit != null){
                    paintExit(_Graph, exit.x, exit.y);
                }

                //draw agent
                _Graph.setColor(Color.GREEN);
                Point agent = m_Control.getPositionOfAgent();
                if (agent != null){
                    paintAgent(_Graph, agent.x, agent.y);
                }

                //draw objects
                if (m_DrawUnseenObjects){
                    _Graph.setColor(Color.BLUE);
                    ArrayList<Point> objs = m_Control.getPositionOfObjects();
                    if (!objs.isEmpty()){
                        for (Point obj: objs){
                            paintObject(_Graph, obj.x, obj.y);
                        }
                    }
                }
                
                //draw seen objects
                _Graph.setColor(Color.RED);
                points = m_Control.getAgentSeenObjects();
                if (points != null){
                    if (!points.isEmpty()){
                        for (Point obj: points){
                            paintObject(_Graph, obj.x, obj.y);
                        }
                    }
                }
                
                _Graph.setColor(Color.getHSBColor( (float)30.76, (float)100.0, (float)57.25));
                if (m_Control.isGameOver()){
                    paintGameOver(_Graph);
                }
            }
        }
    }

    private class RunWorker extends SwingWorker<Control, Control>{
        
        private final boolean m_WithoutDelay;
        private final boolean m_ShowUnseenObjects;
        int m_x;
        int m_y;
        
        public RunWorker(boolean _WithoutDelay, boolean _ShowUnseenObjects) {
            m_WithoutDelay = _WithoutDelay;
            m_ShowUnseenObjects = _ShowUnseenObjects;
            m_x = 0;
            m_y = 0;
        }
        
        @Override
        @SuppressWarnings("SleepWhileInLoop")
        protected Control doInBackground() throws Exception {
            while (! this.isCancelled() && ! Control.getInstance().isGameOver()){
                Control.getInstance().nextStep();
                publish(Control.getInstance());
                
                try {
                    if (m_WithoutDelay){
                        Thread.sleep(50);
                    }
                    else{
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException | CancellationException ex) {
                    ex.printStackTrace();
                }
            }
            return Control.getInstance();
        }

        @Override
        protected void done() {
            try {
                if (! this.isCancelled()){
                    Control control = get();
                    if (control.isGameOver()){
                        setGameState(GameState.HAS_GAME);
                    }
                }
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
        }
        
        @Override
        protected void process(List<Control> chunks) {
            for (Control control: chunks){
                ((GamePanel)pnlGameField).redraw(control, m_ShowUnseenObjects);
            }
        }
        
    }
    
/* ---- METHODS ---- */
    /**
     * Set buttons on start state
     */	    
    private void setButtonsStart(){
        btnGenerate.setEnabled(true);
        btnClearMaze.setEnabled(true);
        btnLoadMaze.setEnabled(true);
        btnSaveMaze.setEnabled(false);
        
        rbtDefault.setEnabled(true);
        rbtDistance.setEnabled(true);
        rbtObjects.setEnabled(true);
        rbtDistanceObjects.setEnabled(true);
        
        rbtAllSeen.setEnabled(true);
        rbtCurrentPosition.setEnabled(true);
        rbtCurrentSeen.setEnabled(true);
        
        spnHeight.setEnabled(true);
        spnWidth.setEnabled(true);
        spnSight.setEnabled(true);
        spnNumOfObjects.setEnabled(true);
        
        btnNewGame.setEnabled(true);
        btnClearGame.setEnabled(false);
        btnLoadGame.setEnabled(true);
        btnSaveGame.setEnabled(false);
        btnStop.setEnabled(false);
        btnStep.setEnabled(false);
        btnRun.setEnabled(false);
        cbWithoutDelay.setEnabled(true);
        cbShowUnseenObjects.setEnabled(true);
    }   
    /**
     * Set buttons on has maze state
     */	 
    private void setButtonsHasMaze(){
        btnGenerate.setEnabled(true);
        btnClearMaze.setEnabled(true);
        btnLoadMaze.setEnabled(true);
        btnSaveMaze.setEnabled(true);
        
        rbtDefault.setEnabled(true);
        rbtDistance.setEnabled(true);
        rbtObjects.setEnabled(true);
        rbtDistanceObjects.setEnabled(true);
        
        rbtAllSeen.setEnabled(true);
        rbtCurrentPosition.setEnabled(true);
        rbtCurrentSeen.setEnabled(true);
        
        spnHeight.setEnabled(true);
        spnWidth.setEnabled(true);
        spnSight.setEnabled(true);
        spnNumOfObjects.setEnabled(true);
        
        btnNewGame.setEnabled(true);
        btnClearGame.setEnabled(false);
        btnLoadGame.setEnabled(true);
        btnSaveGame.setEnabled(false);
        btnStop.setEnabled(false);
        btnStep.setEnabled(false);
        btnRun.setEnabled(false);
        cbWithoutDelay.setEnabled(true);
        cbShowUnseenObjects.setEnabled(true);
    }  
    /**
     * Set buttons on has game state
     */	 
    private void setButtonsHasGame(){
        btnGenerate.setEnabled(false);
        btnClearMaze.setEnabled(false);
        btnLoadMaze.setEnabled(false);
        btnSaveMaze.setEnabled(true);
        
        rbtDefault.setEnabled(false);
        rbtDistance.setEnabled(false);
        rbtObjects.setEnabled(false);
        rbtDistanceObjects.setEnabled(false);
        
        rbtAllSeen.setEnabled(false);
        rbtCurrentPosition.setEnabled(false);
        rbtCurrentSeen.setEnabled(false);
        
        spnHeight.setEnabled(false);
        spnWidth.setEnabled(false);
        spnSight.setEnabled(false);
        spnNumOfObjects.setEnabled(false);
        
        btnNewGame.setEnabled(true);
        btnClearGame.setEnabled(true);
        btnLoadGame.setEnabled(true);
        btnSaveGame.setEnabled(true);
        btnStop.setEnabled(false);
        btnStep.setEnabled(true);
        btnRun.setEnabled(true); 
        cbWithoutDelay.setEnabled(true);
        cbShowUnseenObjects.setEnabled(true);
    }  
    /**
     * Set buttons on game inprogress state
     */	 
    private void setButtonsInProgress(){
        btnGenerate.setEnabled(false);
        btnClearMaze.setEnabled(false);
        btnLoadMaze.setEnabled(false);
        btnSaveMaze.setEnabled(true);
        
        rbtDefault.setEnabled(false);
        rbtDistance.setEnabled(false);
        rbtObjects.setEnabled(false);
        rbtDistanceObjects.setEnabled(false);
        
        rbtAllSeen.setEnabled(false);
        rbtCurrentPosition.setEnabled(false);
        rbtCurrentSeen.setEnabled(false);
        
        spnHeight.setEnabled(false);
        spnWidth.setEnabled(false);
        spnSight.setEnabled(false);
        spnNumOfObjects.setEnabled(false);
        
        btnNewGame.setEnabled(false);
        btnClearGame.setEnabled(true);
        btnLoadGame.setEnabled(false);
        btnSaveGame.setEnabled(true);
        btnStop.setEnabled(m_AutoRunning);
        btnStep.setEnabled(!m_AutoRunning);
        btnRun.setEnabled(!m_AutoRunning);    
        cbWithoutDelay.setEnabled(!m_AutoRunning);
        cbShowUnseenObjects.setEnabled(!m_AutoRunning);
    }
    
    /**
     * SetGameState
     */	    
    private void setGameState(GameState _NewState){
        m_State = _NewState;
        
        switch (_NewState) {
            case DEFAULT:
                setButtonsStart();
                break;
            case HAS_MAZE:
                setButtonsHasMaze();
                break;
            case HAS_GAME:
                setButtonsHasGame();
                break;
            case IN_PROGRESS:
                setButtonsInProgress();
                break;
        }     
    }
    
    private Heuristic getHeuristic(){
        if (rbtDistance.isSelected()){
            return Heuristic.DISTANCE;
        }
        else if (rbtDistanceObjects.isSelected()){
            return Heuristic.DISTANCE_OBJECT;
        }
        else if (rbtObjects.isSelected()){
            return Heuristic.OBJECT;
        }
        else{
            return Heuristic.DEFAULT;
        }
    }
    
    private Algorithm getAlgorithm(){
        if (rbtAllSeen.isSelected()){
            return Algorithm.ALL_SEEN;
        }
        else if (rbtCurrentSeen.isSelected()){
            return Algorithm.CURRENT_SEEN;
        }
        else{
            return Algorithm.CURRENT_POSITION;
        }
    }
    
    private void generate(int _Width, int _Height){
        Maze.getInstance().setSize(_Width, _Height);
        Maze.getInstance().generate();
    }
    
    private void loadMaze(){
        Maze.getInstance().loadFromJSON();
    }
    
    private void saveMaze(){
        Maze.getInstance().saveToJSON();
    }
    
    private void loadGame(){
        //+ Control.Load
        //todo
    }
    
    private void saveGame(){
        //Control.Save
        //todo
    }
    
    private void newGame(int _Width, int _Height, int _NumOfObjects, int _Sight){
        if (Maze.getInstance().getIsEmpty()){
            generate(_Width, _Height);
        }
        
        Control.getInstance().init( (int)(spnSight.getValue()), (int)(spnNumOfObjects.getValue()), getHeuristic(), getAlgorithm());
    }
    
    private void clearGame(){
        Control.getInstance().clear();
        
        if (m_RunWorker != null){
            if (m_AutoRunning){
                m_RunWorker.cancel(false);
                m_AutoRunning = false;
            }
            
            m_RunWorker = null;
        }
    }
    
    private void stop(){
        m_RunWorker.cancel(false);
        m_AutoRunning = false;
    }
    
    private void step(){
        Control.getInstance().nextStep();
    }
                
    private void run(){
        m_RunWorker = new RunWorker(cbWithoutDelay.isSelected(), cbShowUnseenObjects.isSelected());
        m_RunWorker.execute();
        m_AutoRunning = true;
    }

/* ---- Actions and auto-generated codes ---- */
    /**
     * generate
     */	
    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // Generate a new labyrinth by the given size
        generate( (int)(spnHeight.getValue()), (int)(spnWidth.getValue()) );       
        
        //Setting gamestate
        if (m_State == GameState.DEFAULT){
            setGameState(GameState.HAS_MAZE);
        }
        
        //Redrawing gamefield
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void btnClearMazeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearMazeActionPerformed
        if (m_State == GameState.DEFAULT || m_State == GameState.HAS_MAZE){
            Maze.getInstance().empty();
        }
        
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
    }//GEN-LAST:event_btnClearMazeActionPerformed

    /**
     * LoadMaze
     */	
    private void btnLoadMazeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadMazeActionPerformed
        // LOAD LABYRINTH TO FILE
        JFileChooser fileChooser = new JFileChooser();
        
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){            
            if (Maze.getInstance().loadFromJSON(fileChooser.getSelectedFile().getPath())){
                //Setting gamestate
                if (m_State == GameState.DEFAULT){
                    setGameState(GameState.HAS_MAZE);
                }
                
                //Redrawing gamefield
                ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
            }
            else{
                JOptionPane.showMessageDialog(null, "Loading failed. The file doesn't exist, can't be read or broken!");
            }
        }        
    }//GEN-LAST:event_btnLoadMazeActionPerformed

    /**
     * SaveMaze
     */	
    private void btnSaveMazeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveMazeActionPerformed
        // SAVE LABYRINTH TO FILE
        JFileChooser fileChooser = new JFileChooser();
        
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            if (Maze.getInstance().getIsEmpty()){
                JOptionPane.showMessageDialog(null, "Saving failed: maze was empty!");
            }
            else if (Maze.getInstance().saveToJSON(fileChooser.getSelectedFile().getPath())){
                JOptionPane.showMessageDialog(null, "Saving finished!");
            }
            else{
                JOptionPane.showMessageDialog(null, "Saving failed: the file can't be modified or created!"); 
            }
        }       
    }//GEN-LAST:event_btnSaveMazeActionPerformed

    /**
     * NewGame
     */		
    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        //Create a new game by the given parameters.
        newGame((int)(spnHeight.getValue()),
                (int)(spnWidth.getValue()),
                (int)(spnNumOfObjects.getValue()),
                (int)(spnSight.getValue())
                );
        
        //Setting gamestate
        if (m_State == GameState.DEFAULT || m_State == GameState.HAS_MAZE){
            setGameState(GameState.HAS_GAME);
        }
        
        //Redrawing gamefield
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
    }//GEN-LAST:event_btnNewGameActionPerformed

    /**
     * ClearGame
     */	
    private void btnClearGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearGameActionPerformed
        //Clear current game
        clearGame();
        
        //Setting gamestate
        if (m_State == GameState.HAS_GAME || m_State == GameState.IN_PROGRESS){
            setGameState(GameState.HAS_MAZE);
        }
        
        //Redrawing gamefield
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
    }//GEN-LAST:event_btnClearGameActionPerformed

    /**
     * LoadGame
     */		
    private void btnLoadGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadGameActionPerformed
        loadGame();
        
        //Redrawing gamefield
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
        
        //Setting gamestate
        if (m_State == GameState.DEFAULT || m_State == GameState.HAS_MAZE){
            setGameState(GameState.HAS_GAME);
        }
    }//GEN-LAST:event_btnLoadGameActionPerformed

    /**
     * SaveGame
     */	
    private void btnSaveGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveGameActionPerformed
        saveGame();
    }//GEN-LAST:event_btnSaveGameActionPerformed

    /**
     * Stop
     */		
    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
        //Stop the currently running game
        stop();
        
        //Called to set the buttons
        setGameState(m_State);
    }//GEN-LAST:event_btnStopActionPerformed

    /**
     * Step
     */	
    private void btnStepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStepActionPerformed
        step();
        
        //Redrawing gamefield
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
        
        //Setting gamestate
        if (m_State == GameState.HAS_GAME){
            setGameState(GameState.IN_PROGRESS);
        }
    }//GEN-LAST:event_btnStepActionPerformed

    /**
     * Run
     */	   
    private void btnRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunActionPerformed
        run();
        
        if (m_State == GameState.HAS_GAME){
            setGameState(GameState.IN_PROGRESS);
        }
        else{
            setGameState(m_State);
        }
    }//GEN-LAST:event_btnRunActionPerformed

	
    /**
     * Show unseen objects change
     */
    private void cbShowUnseenObjectsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbShowUnseenObjectsActionPerformed
        ((GamePanel)pnlGameField).redraw(Control.getInstance(), cbShowUnseenObjects.isSelected());
    }//GEN-LAST:event_cbShowUnseenObjectsActionPerformed
	
    /**
     * Exit
     * 
     * Action to exit the application.
     */	
    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        if (JOptionPane.YES_OPTION == JOptionPane.showOptionDialog(
                null,
                "Are you sure you want to quit?", "Exit",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null,
                JOptionPane.NO_OPTION)
            ){
            
            System.exit(0);
        }
    }//GEN-LAST:event_btnExitActionPerformed
   
	
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
	
   
    /**
     * Creates new form Main
     */
    public Main() {
        m_State = GameState.DEFAULT;
        m_AutoRunning = false;
        
        initComponents();
        setGameState(GameState.DEFAULT);
        ((GamePanel)pnlGameField).saveSize();
        Maze.getInstance().setNeedAlternateRoutes(true);
    }
    
    
    //<editor-fold defaultstate="collapsed" desc=" All the generated code">
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        btgHeuristics = new javax.swing.ButtonGroup();
        btgAlgorithms = new javax.swing.ButtonGroup();
        pnlOptions = new javax.swing.JPanel();
        pnlAgentInfos = new javax.swing.JPanel();
        rbtCurrentPosition = new javax.swing.JRadioButton();
        rbtCurrentSeen = new javax.swing.JRadioButton();
        rbtAllSeen = new javax.swing.JRadioButton();
        pnlMazeOptions = new javax.swing.JPanel();
        pnlMazeSize = new javax.swing.JPanel();
        lblVertical = new javax.swing.JLabel();
        spnHeight = new javax.swing.JSpinner();
        lblHorizontal = new javax.swing.JLabel();
        spnWidth = new javax.swing.JSpinner();
        pnlMazeButtons = new javax.swing.JPanel();
        btnGenerate = new javax.swing.JButton();
        btnLoadMaze = new javax.swing.JButton();
        btnSaveMaze = new javax.swing.JButton();
        btnClearMaze = new javax.swing.JButton();
        pnlHeuristics = new javax.swing.JPanel();
        rbtDefault = new javax.swing.JRadioButton();
        rbtDistance = new javax.swing.JRadioButton();
        rbtObjects = new javax.swing.JRadioButton();
        rbtDistanceObjects = new javax.swing.JRadioButton();
        pnlControls = new javax.swing.JPanel();
        btnStop = new javax.swing.JButton();
        btnStep = new javax.swing.JButton();
        btnRun = new javax.swing.JButton();
        cbWithoutDelay = new javax.swing.JCheckBox();
        lblNumOfObjects = new javax.swing.JLabel();
        spnNumOfObjects = new javax.swing.JSpinner();
        btnNewGame = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        btnSaveGame = new javax.swing.JButton();
        btnLoadGame = new javax.swing.JButton();
        lblSight = new javax.swing.JLabel();
        spnSight = new javax.swing.JSpinner();
        btnClearGame = new javax.swing.JButton();
        cbShowUnseenObjects = new javax.swing.JCheckBox();
        pnlGameField = new GamePanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Miantiji");
        setName("Form"); // NOI18N
        setResizable(false);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        pnlOptions.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlOptions.setMinimumSize(new java.awt.Dimension(500, 500));
        pnlOptions.setName("pnlOptions"); // NOI18N
        pnlOptions.setPreferredSize(new java.awt.Dimension(500, 500));
        pnlOptions.setLayout(new java.awt.GridBagLayout());

        pnlAgentInfos.setBorder(javax.swing.BorderFactory.createTitledBorder("Usable algorithms"));
        pnlAgentInfos.setMinimumSize(new java.awt.Dimension(475, 150));
        pnlAgentInfos.setName("pnlAgentInfos"); // NOI18N
        pnlAgentInfos.setPreferredSize(new java.awt.Dimension(50, 270));
        pnlAgentInfos.setLayout(new java.awt.GridBagLayout());

        btgAlgorithms.add(rbtCurrentPosition);
        rbtCurrentPosition.setSelected(true);
        rbtCurrentPosition.setText("<html>The algorithm will count the depth of the representation graph from the agent current position.</html>");
        rbtCurrentPosition.setFocusCycleRoot(true);
        rbtCurrentPosition.setMaximumSize(new java.awt.Dimension(231658, 23));
        rbtCurrentPosition.setMinimumSize(new java.awt.Dimension(450, 30));
        rbtCurrentPosition.setName("rbtCurrentPosition"); // NOI18N
        rbtCurrentPosition.setNextFocusableComponent(rbtCurrentSeen);
        rbtCurrentPosition.setPreferredSize(new java.awt.Dimension(500, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        pnlAgentInfos.add(rbtCurrentPosition, gridBagConstraints);

        btgAlgorithms.add(rbtCurrentSeen);
        rbtCurrentSeen.setText("<html>The algorithm will count the depth of the representation graph from the fields which are currently seen by the agent.</html>");
        rbtCurrentSeen.setName("rbtCurrentSeen"); // NOI18N
        rbtCurrentSeen.setNextFocusableComponent(rbtAllSeen);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        pnlAgentInfos.add(rbtCurrentSeen, gridBagConstraints);

        btgAlgorithms.add(rbtAllSeen);
        rbtAllSeen.setText("<html>The algorithm will count the depth of the representation graph from the fields which were already seen by the agent.<html>");
        rbtAllSeen.setName("rbtAllSeen"); // NOI18N
        rbtAllSeen.setNextFocusableComponent(spnHeight);
        rbtAllSeen.setPreferredSize(new java.awt.Dimension(400, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        pnlAgentInfos.add(rbtAllSeen, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnlOptions.add(pnlAgentInfos, gridBagConstraints);

        pnlMazeOptions.setBorder(javax.swing.BorderFactory.createTitledBorder("Maze options"));
        pnlMazeOptions.setMinimumSize(new java.awt.Dimension(250, 180));
        pnlMazeOptions.setName("pnlMazeOptions"); // NOI18N
        pnlMazeOptions.setPreferredSize(new java.awt.Dimension(572, 175));
        pnlMazeOptions.setLayout(new java.awt.GridBagLayout());

        pnlMazeSize.setName("pnlMazeSize"); // NOI18N
        pnlMazeSize.setPreferredSize(new java.awt.Dimension(275, 80));
        pnlMazeSize.setLayout(new java.awt.GridBagLayout());

        lblVertical.setText("Vertical size of the maze:");
        lblVertical.setName("lblVertical"); // NOI18N
        lblVertical.setPreferredSize(new java.awt.Dimension(100, 14));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 58;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeSize.add(lblVertical, gridBagConstraints);

        spnHeight.setModel(new javax.swing.SpinnerNumberModel(15, 5, 50, 1));
        spnHeight.setName("spnHeight"); // NOI18N
        spnHeight.setNextFocusableComponent(spnWidth);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = -2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeSize.add(spnHeight, gridBagConstraints);

        lblHorizontal.setText("Horizontal size of the maze:");
        lblHorizontal.setMaximumSize(new java.awt.Dimension(120, 14));
        lblHorizontal.setMinimumSize(new java.awt.Dimension(100, 14));
        lblHorizontal.setName("lblHorizontal"); // NOI18N
        lblHorizontal.setPreferredSize(new java.awt.Dimension(120, 14));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 45;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeSize.add(lblHorizontal, gridBagConstraints);

        spnWidth.setModel(new javax.swing.SpinnerNumberModel(15, 5, 50, 1));
        spnWidth.setName("spnWidth"); // NOI18N
        spnWidth.setNextFocusableComponent(btnGenerate);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = -2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeSize.add(spnWidth, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        pnlMazeOptions.add(pnlMazeSize, gridBagConstraints);

        pnlMazeButtons.setMinimumSize(new java.awt.Dimension(220, 70));
        pnlMazeButtons.setName("pnlMazeButtons"); // NOI18N
        pnlMazeButtons.setPreferredSize(new java.awt.Dimension(200, 70));
        pnlMazeButtons.setLayout(new java.awt.GridBagLayout());

        btnGenerate.setText("Generate");
        btnGenerate.setMaximumSize(new java.awt.Dimension(1000, 2311));
        btnGenerate.setMinimumSize(new java.awt.Dimension(80, 25));
        btnGenerate.setName("btnGenerate"); // NOI18N
        btnGenerate.setNextFocusableComponent(btnClearMaze);
        btnGenerate.setPreferredSize(new java.awt.Dimension(77, 30));
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeButtons.add(btnGenerate, gridBagConstraints);

        btnLoadMaze.setText("Load Maze");
        btnLoadMaze.setMargin(new java.awt.Insets(2, 5, 2, 5));
        btnLoadMaze.setMaximumSize(new java.awt.Dimension(5500, 2300));
        btnLoadMaze.setMinimumSize(new java.awt.Dimension(80, 25));
        btnLoadMaze.setName("btnLoadMaze"); // NOI18N
        btnLoadMaze.setNextFocusableComponent(btnSaveMaze);
        btnLoadMaze.setPreferredSize(new java.awt.Dimension(70, 23));
        btnLoadMaze.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadMazeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeButtons.add(btnLoadMaze, gridBagConstraints);

        btnSaveMaze.setText("Save maze");
        btnSaveMaze.setMargin(new java.awt.Insets(2, 5, 2, 5));
        btnSaveMaze.setMaximumSize(new java.awt.Dimension(5700, 2500));
        btnSaveMaze.setMinimumSize(new java.awt.Dimension(80, 25));
        btnSaveMaze.setName("btnSaveMaze"); // NOI18N
        btnSaveMaze.setNextFocusableComponent(rbtDefault);
        btnSaveMaze.setPreferredSize(new java.awt.Dimension(70, 23));
        btnSaveMaze.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveMazeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeButtons.add(btnSaveMaze, gridBagConstraints);

        btnClearMaze.setText("Clear");
        btnClearMaze.setMaximumSize(new java.awt.Dimension(80, 23));
        btnClearMaze.setMinimumSize(new java.awt.Dimension(80, 23));
        btnClearMaze.setName("btnClearMaze"); // NOI18N
        btnClearMaze.setNextFocusableComponent(btnLoadMaze);
        btnClearMaze.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearMazeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlMazeButtons.add(btnClearMaze, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        pnlMazeOptions.add(pnlMazeButtons, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        pnlOptions.add(pnlMazeOptions, gridBagConstraints);

        pnlHeuristics.setBorder(javax.swing.BorderFactory.createTitledBorder("Usable heuristics"));
        pnlHeuristics.setMinimumSize(new java.awt.Dimension(225, 180));
        pnlHeuristics.setName("pnlHeuristics"); // NOI18N
        pnlHeuristics.setLayout(new java.awt.GridLayout(4, 1));

        btgHeuristics.add(rbtDefault);
        rbtDefault.setSelected(true);
        rbtDefault.setText("Default heuristic");
        rbtDefault.setName("rbtDefault"); // NOI18N
        rbtDefault.setNextFocusableComponent(spnNumOfObjects);
        pnlHeuristics.add(rbtDefault);

        btgHeuristics.add(rbtDistance);
        rbtDistance.setText("<html>Heuristic depending on the distance from the exit</html>");
        rbtDistance.setName("rbtDistance"); // NOI18N
        rbtDistance.setNextFocusableComponent(spnNumOfObjects);
        pnlHeuristics.add(rbtDistance);

        btgHeuristics.add(rbtObjects);
        rbtObjects.setText("Heuristic to avoid the seen objects");
        rbtObjects.setName("rbtObjects"); // NOI18N
        rbtObjects.setNextFocusableComponent(spnNumOfObjects);
        pnlHeuristics.add(rbtObjects);

        btgHeuristics.add(rbtDistanceObjects);
        rbtDistanceObjects.setText("<html>Heuristic to avoid the seen objects and depending on the distance from the exit</html>");
        rbtDistanceObjects.setName("rbtDistanceObjects"); // NOI18N
        rbtDistanceObjects.setNextFocusableComponent(spnNumOfObjects);
        pnlHeuristics.add(rbtDistanceObjects);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        pnlOptions.add(pnlHeuristics, gridBagConstraints);

        pnlControls.setBorder(javax.swing.BorderFactory.createTitledBorder("Controls"));
        pnlControls.setMinimumSize(new java.awt.Dimension(475, 135));
        pnlControls.setName("pnlControls"); // NOI18N
        pnlControls.setPreferredSize(new java.awt.Dimension(450, 134));
        pnlControls.setLayout(new java.awt.GridBagLayout());

        btnStop.setText("STOP");
        btnStop.setName("btnStop"); // NOI18N
        btnStop.setNextFocusableComponent(btnStep);
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnStop, gridBagConstraints);

        btnStep.setText("STEP");
        btnStep.setName("btnStep"); // NOI18N
        btnStep.setNextFocusableComponent(cbWithoutDelay);
        btnStep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStepActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnStep, gridBagConstraints);

        btnRun.setText("RUN");
        btnRun.setMinimumSize(new java.awt.Dimension(57, 23));
        btnRun.setName("btnRun"); // NOI18N
        btnRun.setNextFocusableComponent(btnExit);
        btnRun.setPreferredSize(new java.awt.Dimension(57, 23));
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnRun, gridBagConstraints);

        cbWithoutDelay.setText("Run without delay");
        cbWithoutDelay.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cbWithoutDelay.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        cbWithoutDelay.setMaximumSize(new java.awt.Dimension(115, 23));
        cbWithoutDelay.setMinimumSize(new java.awt.Dimension(115, 23));
        cbWithoutDelay.setName("cbWithoutDelay"); // NOI18N
        cbWithoutDelay.setNextFocusableComponent(btnRun);
        cbWithoutDelay.setPreferredSize(new java.awt.Dimension(115, 23));
        cbWithoutDelay.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(cbWithoutDelay, gridBagConstraints);

        lblNumOfObjects.setText("Number of objects:");
        lblNumOfObjects.setName("lblNumOfObjects"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(lblNumOfObjects, gridBagConstraints);

        spnNumOfObjects.setModel(new javax.swing.SpinnerNumberModel(0, 0, 15, 1));
        spnNumOfObjects.setMinimumSize(new java.awt.Dimension(40, 20));
        spnNumOfObjects.setName("spnNumOfObjects"); // NOI18N
        spnNumOfObjects.setNextFocusableComponent(spnSight);
        spnNumOfObjects.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(spnNumOfObjects, gridBagConstraints);

        btnNewGame.setText("New game");
        btnNewGame.setMargin(new java.awt.Insets(2, 5, 2, 5));
        btnNewGame.setMaximumSize(new java.awt.Dimension(100, 50));
        btnNewGame.setMinimumSize(new java.awt.Dimension(75, 25));
        btnNewGame.setName("btnNewGame"); // NOI18N
        btnNewGame.setNextFocusableComponent(btnClearGame);
        btnNewGame.setPreferredSize(new java.awt.Dimension(75, 23));
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnNewGame, gridBagConstraints);

        btnExit.setText("Exit");
        btnExit.setMaximumSize(new java.awt.Dimension(75, 23));
        btnExit.setName("btnExit"); // NOI18N
        btnExit.setNextFocusableComponent(rbtCurrentPosition);
        btnExit.setPreferredSize(new java.awt.Dimension(75, 25));
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnExit, gridBagConstraints);

        btnSaveGame.setText("Save game");
        btnSaveGame.setMargin(new java.awt.Insets(2, 7, 2, 7));
        btnSaveGame.setMaximumSize(new java.awt.Dimension(75, 23));
        btnSaveGame.setMinimumSize(new java.awt.Dimension(75, 23));
        btnSaveGame.setName("btnSaveGame"); // NOI18N
        btnSaveGame.setNextFocusableComponent(btnStop);
        btnSaveGame.setPreferredSize(new java.awt.Dimension(75, 23));
        btnSaveGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveGameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnSaveGame, gridBagConstraints);

        btnLoadGame.setText("Load game");
        btnLoadGame.setMargin(new java.awt.Insets(2, 7, 2, 7));
        btnLoadGame.setMaximumSize(new java.awt.Dimension(75, 23));
        btnLoadGame.setMinimumSize(new java.awt.Dimension(75, 23));
        btnLoadGame.setName("btnLoadGame"); // NOI18N
        btnLoadGame.setNextFocusableComponent(btnSaveGame);
        btnLoadGame.setPreferredSize(new java.awt.Dimension(75, 23));
        btnLoadGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadGameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnLoadGame, gridBagConstraints);

        lblSight.setText("Distance of sight:");
        lblSight.setName("lblSight"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(lblSight, gridBagConstraints);

        spnSight.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        spnSight.setMinimumSize(new java.awt.Dimension(40, 20));
        spnSight.setName("spnSight"); // NOI18N
        spnSight.setNextFocusableComponent(btnNewGame);
        spnSight.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(spnSight, gridBagConstraints);

        btnClearGame.setText("Clear game");
        btnClearGame.setMargin(new java.awt.Insets(2, 5, 2, 5));
        btnClearGame.setMaximumSize(new java.awt.Dimension(75, 23));
        btnClearGame.setMinimumSize(new java.awt.Dimension(75, 23));
        btnClearGame.setName("btnClearGame"); // NOI18N
        btnClearGame.setNextFocusableComponent(btnLoadGame);
        btnClearGame.setPreferredSize(new java.awt.Dimension(75, 23));
        btnClearGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearGameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnlControls.add(btnClearGame, gridBagConstraints);

        cbShowUnseenObjects.setSelected(true);
        cbShowUnseenObjects.setText("Show unseen objects");
        cbShowUnseenObjects.setName("cbShowUnseenObjects"); // NOI18N
        cbShowUnseenObjects.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbShowUnseenObjectsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        pnlControls.add(cbShowUnseenObjects, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LAST_LINE_START;
        pnlOptions.add(pnlControls, gridBagConstraints);

        getContentPane().add(pnlOptions);

        pnlGameField.setBackground(new java.awt.Color(255, 255, 255));
        pnlGameField.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(0, 0, 0), new java.awt.Color(102, 102, 102)));
        pnlGameField.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pnlGameField.setMinimumSize(new java.awt.Dimension(500, 500));
        pnlGameField.setName("pnlGameField"); // NOI18N
        pnlGameField.setPreferredSize(new java.awt.Dimension(500, 500));

        javax.swing.GroupLayout pnlGameFieldLayout = new javax.swing.GroupLayout(pnlGameField);
        pnlGameField.setLayout(pnlGameFieldLayout);
        pnlGameFieldLayout.setHorizontalGroup(
            pnlGameFieldLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 496, Short.MAX_VALUE)
        );
        pnlGameFieldLayout.setVerticalGroup(
            pnlGameFieldLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 496, Short.MAX_VALUE)
        );

        getContentPane().add(pnlGameField);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btgAlgorithms;
    private javax.swing.ButtonGroup btgHeuristics;
    private javax.swing.JButton btnClearGame;
    private javax.swing.JButton btnClearMaze;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnLoadGame;
    private javax.swing.JButton btnLoadMaze;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton btnSaveGame;
    private javax.swing.JButton btnSaveMaze;
    private javax.swing.JButton btnStep;
    private javax.swing.JButton btnStop;
    private javax.swing.JCheckBox cbShowUnseenObjects;
    private javax.swing.JCheckBox cbWithoutDelay;
    private javax.swing.JLabel lblHorizontal;
    private javax.swing.JLabel lblNumOfObjects;
    private javax.swing.JLabel lblSight;
    private javax.swing.JLabel lblVertical;
    private javax.swing.JPanel pnlAgentInfos;
    private javax.swing.JPanel pnlControls;
    private javax.swing.JPanel pnlGameField;
    private javax.swing.JPanel pnlHeuristics;
    private javax.swing.JPanel pnlMazeButtons;
    private javax.swing.JPanel pnlMazeOptions;
    private javax.swing.JPanel pnlMazeSize;
    private javax.swing.JPanel pnlOptions;
    private javax.swing.JRadioButton rbtAllSeen;
    private javax.swing.JRadioButton rbtCurrentPosition;
    private javax.swing.JRadioButton rbtCurrentSeen;
    private javax.swing.JRadioButton rbtDefault;
    private javax.swing.JRadioButton rbtDistance;
    private javax.swing.JRadioButton rbtDistanceObjects;
    private javax.swing.JRadioButton rbtObjects;
    private javax.swing.JSpinner spnHeight;
    private javax.swing.JSpinner spnNumOfObjects;
    private javax.swing.JSpinner spnSight;
    private javax.swing.JSpinner spnWidth;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>
}
